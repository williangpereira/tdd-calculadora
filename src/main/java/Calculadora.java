public class Calculadora {



    public Calculadora(){}

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double somaFlutuante(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public int divisaoInteiro(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public int divisaoNegativos(int primeiroNumero, int segundoNumero){
        int resultado = (primeiroNumero) / (segundoNumero);
        return resultado;
    }

    public float divisaoFlutuante(float primeiroNumero, float segundoNumero){
        float resultado = (primeiroNumero) / (segundoNumero);
        return resultado;
    }

    public int multiplicacao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public int multiplicacaoNegativo(int primeiroNumero, int segundoNumero){
        int resultado = (primeiroNumero) * (segundoNumero);
        return resultado;
    }

    public float multiPlicacaoFlutuante(float primeiroNumero, float segundoNumero){
        float resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

}
