import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Dictionary;
import java.util.concurrent.atomic.DoubleAccumulator;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }


    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.somaFlutuante(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros(){
        int resultado = calculadora.divisaoInteiro(5, 1);

        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosNegativos(){
        double resultado = calculadora.divisaoNegativos(-5, -2);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosFlutuantes(){
        float resultado = calculadora.divisaoFlutuante(2,3);

        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros(){
        double resultado = calculadora.multiplicacao(2,2);

        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosNegativos(){
        int resultado = calculadora.multiplicacaoNegativo(5,-10);

        Assertions.assertEquals(-50, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes(){
        float resultado = calculadora.multiPlicacaoFlutuante(4, 3);

        Assertions.assertEquals(12,resultado);
    }

}
